<?php
  $yhteys = mysqli_connect("localhost", "root", "");
  if(!mysqli_select_db($yhteys , "uutissivusto"))
    exit("Tietokannan valinta epäonnistui.");
  mysqli_set_charset($yhteys,"utf8");
  $sql = "select * from blogi, blogikirjoitus where blogi.blogi_id = blogikirjoitus.blogi_id order by julkaisuaika desc";
  $tulos = mysqli_query($yhteys , $sql);
  $blogit = array();
  while($temp = mysqli_fetch_assoc($tulos)) {
    $blogit[] = json_encode($temp);

  }
  header('Content-Type: application/json; charset=UTF-8');
  $json = json_encode($blogit);
  echo $json;
 ?>
