<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title></title>
  </head>
  <body>
    <div id="wrapper">
    <div id="container">

      <div id="header">
        <h1>Satuvaltakunnan tarinat</h1>

        <p>Uutisia lumotusta maasta</p>

      </div>

      <div class="saaviikko">
        <p>Viikon sää - viikko 21</p>
      </div>
      <div class="saa">

      <?php

        $xml=simplexml_load_file("http://localhost/php/portfolio1/saa.php?vko=21") or die("Virhe: XML-syötteen käsittely epäonnistui");

        foreach ($xml->children() as $saa) {
          echo "<div class = 'paivansaa'>";
          echo "<p class = 'paiva'>";
          $date=date_create($saa->paiva);
          echo date_format($date,"d.m.Y ");;
          echo "</p>";
          echo "<p class = 'lampotila'>";
          echo $saa->lampotila . "&deg;" . "C";
          echo "</p>";
          echo "<p class = 'saatila'>";
          echo "<b>";
          echo "Säätila: " . "<br> ";
          echo "</b>";
          echo $saa->saatila . "<br> ";
          echo "</p>";
          echo "<p class = 'tuuli'>";
          echo "<b>";
          echo "Tuulennopeus: " . "<br> ";
          echo "</b>";
          echo $saa->tuulennopeus . "m/s";
          echo "</p>";
          echo "</div>";
        }

       ?>
       </div>

       </div>

       <div id="text">

         <div class="main-text">



         <?php
         require_once("haku3.php");

         while($uutinen = mysqli_fetch_assoc($tulos))
         {
           echo "<div class='teksti'>";
           echo "<p class = 'teksti_nimi'>";
           echo "<b>";
           echo $uutinen["otsikko"] . '<br>';
           echo "</b>";
           echo "</p>";
           echo "<p class = 'uutinen_aika'>";
           $date=date_create($uutinen['julkaisuaika']);
           echo date_format($date,"d.m.Y ");
           echo "klo ";
           $date2=date_create($uutinen['julkaisuaika']);
           echo date_format($date2,"H:i");
           echo "|| ";
           echo $uutinen["kirjoittaja"] . '<br>';
           echo "</p>";
           echo $uutinen["sisalto"] . '<br>';
           echo "</div>";
         }

           ?>

</div>

       <div class="sidepanel">
          <p class="title">Uusimmat uutiset </p>

          <?php

          $sql = "select * from uutiset order by julkaisuaika desc";
          $tulos = mysqli_query($yhteys , $sql);

          while($uutinen = mysqli_fetch_assoc($tulos))
            {
              echo "<div class='blogi'>";
              echo "<p class = 'blogi_nimi'>";
              echo "<b>";
              echo $uutinen["otsikko"] . '<br>';
              echo "</b>";
              echo "</p>";
              echo "<p class = 'uutinen_aika'>";
              $date=date_create($uutinen['julkaisuaika']);
              echo date_format($date,"d.m.Y ");
              echo "klo ";
              $date2=date_create($uutinen['julkaisuaika']);
              echo date_format($date2,"H:i") . "<br>";
              echo "</div>";
            }
           ?>

           <p class="title">Vierailevat kirjoittajat </p>

           <?php

            $json = file_get_contents("http://localhost/php/Portfolio1/haku.php");

            $blogit = json_decode($json, true);

             foreach ($blogit as $blogi) {
               $blogi = json_decode($blogi, true);
               echo "<div class='blogi'>";
               echo "<p class = 'blogi_nimi'>";
               echo "<b>";
               echo  $blogi['nimi'] . ": ";
               echo "</b>";
               echo  $blogi['otsikko']. "<br>";
               echo "</p>";
               echo "<p class = 'blogi_kirjoittaja'>";
               echo  $blogi['kirjoittaja'] . "<br>";
               echo "</p>";
               echo "<p class = 'blogi_julkaisuaika'>";
               $date=date_create($blogi['julkaisuaika']);
               echo date_format($date,"d.m.Y ") . "<br>";
               echo "</p>";
               echo "<br>";
               echo "</div>";
             }
            ?>
        </div>
      </div>
    </div>
  </body>
</html>
