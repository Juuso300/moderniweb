<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <li><a href="index.php">Etusivu</a></li>
          <?php
            require_once "connect.php";

            session_start();

            if (isset($_SESSION['s_id'])) {
              echo '<li class="lista"><a href="post.php">Luo postaus</a></li>';
              echo '<li class="lista"><a href="logout.php">Kirjaudu ulos</a></li>';
            } else {
              echo "<li class='lista'><a href='login.php'>Kirjaudu</a></li>";
              echo "<li class='lista'><a href='register.php'>Rekisteröidy</a></li>";
            }
          ?>
        </ul>
      </div>

      <header>
        <h1>
          <?php
            $cat = mysqli_real_escape_string($yhteys, strip_tags($_GET['cat']));
            echo $cat;
          ?>
        </h1>
      </header>
      <div class="content">
        <?php
          $sql = 'SELECT * FROM kysymys';

          if (isset($_GET['cat'])) {
            $cat = mysqli_real_escape_string($yhteys, strip_tags($_GET['cat']));
            $sql .= " WHERE kategoria = '$cat'";
          }

          $tulokset = $yhteys->query($sql);

          if ($tulokset->num_rows > 0) {

            while ($rivi = $tulokset->fetch_assoc()) {
              $sisalto = $rivi['sisalto'];

              if (strlen($sisalto) > 100) {
                $sisalto = substr($sisalto, 0, 100) . '...';
              }

              echo "<div class ='question'>";
              echo "<a class='link' href = 'view-question.php?ID=" . $rivi['kysymysID'] ."'>";
              echo "<div class='time'>";
              $date = date_create($rivi['paivamaara']);
              echo date_format($date, 'd.m.Y');
              echo "</div>";
              echo "<div class='header'>";
              echo "<b>";
              echo $rivi['otsikko']. '<br>';
              echo "</b>";
              echo "</div>";
              echo "<div class='content2'>";
              echo $sisalto .'<br>';
              echo "</div>";
              echo "</a><br>";
              echo "</div>";
            }
          } else {
            echo "<b>Ei tuloksia</b>";
          }
        ?>
      </div>
    </div>
  </body>
</html>
