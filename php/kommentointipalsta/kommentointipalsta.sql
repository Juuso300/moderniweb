-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11.02.2019 klo 08:25
-- Palvelimen versio: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kommentointipalsta`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `kayttaja`
--

CREATE TABLE `kayttaja` (
  `kayttajaID` int(11) NOT NULL,
  `sahkoposti` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `Salasana` varchar(200) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Vedos taulusta `kayttaja`
--

INSERT INTO `kayttaja` (`kayttajaID`, `sahkoposti`, `nimimerkki`, `Salasana`) VALUES
(2, 'a@gmail.com', 'abc', 'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad'),
(11, 'goijhsh98g@gmail.com', 'giufegb', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4'),
(12, 'abc@gmail.com', 'kissa', 'fb502171be52a780c89aafdfb55d6afe7d0fcfa8b319d77d9053d26076124d87');

-- --------------------------------------------------------

--
-- Rakenne taululle `kysymys`
--

CREATE TABLE `kysymys` (
  `kysymysID` int(11) NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `otsikko` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `sisalto` text COLLATE utf8_swedish_ci NOT NULL,
  `paivamaara` date NOT NULL,
  `kategoria` varchar(50) COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Vedos taulusta `kysymys`
--

INSERT INTO `kysymys` (`kysymysID`, `nimimerkki`, `otsikko`, `sisalto`, `paivamaara`, `kategoria`) VALUES
(1, 'kissa', 'Kuinka saada auto jumiin?', 'wfedgrjhykliöä\'', '2019-01-31', 'autot'),
(2, 'kissa', 'Kaahaavat Skoda-kuskit', 'Vuosia olemme tottuneet siihen, että liikenteen törkykuskien alla on useimmiten bemari, mersu tai audi. Vaan eipähän ole enää, vaan tämän mulle-kaikki-heti-nyt -kuskin valtikan ovat ottaneet itselleen kaahaavat Skoda-kuskit. Näin ainakin täällä pääkaupunkiseudun kaduilla ja ulosmenoväylillä. \r\nSkoda nykyään kelpaa jopa työsuhdeautoksi ja olisikin kiva tietää mikä osa näistä Skoda-törtöistä ajelee nimenomaan työsuhde-Skodalla. Kyllä meidänkin työmaaruokalassa nuoremmat hemmot keskustelevat silmät kiiluen RS-mallin kiihtyvyysarvoista. Kauhulla odotan heidän ilmestymistään liikenteeseen uudella Skodallaan.\r\n\r\nEihän Skodassa mielestäni autona mitään vikaa ole; kelpo tuote myös erilaisten testien perusteella.\r\nIhmetyttää vain, millainen on Skoda-kuski luonteeltaan, kun pitää niin kovasti liikenteessä olla näkyvillä eri tavoin. Eilen iltapäivälläkin yksi Roomster -kuski hermostui Itä-Pasilan Messukeskuksen kohdalla siitä, että kaksikaistaisella tiellä ajoin oikealta hänen ohitseen. Hänen piti oikein torvea soittaa minulle, ja kaahata pari auton mittaa edelleni ja kiilata sieltä edessäni ajaneen Volkkarin eteen. \r\n\r\nSkoda-kuskit näyttävät myös ajavan usein reipasta ylinopeutta eikä punainen liikennevalokaan näytä olevan heille esteenä. Terveisiä vaan viimeviikkoiselle Octavia-kuskille matkallaan Hakamäentietä länteen. Perässä en pysynyt, mutta Vihdintien liikenneympyrässä seisoimme sitten kuitenkin rinnakkain yhtäaikaa.\r\n\r\nEttä sellaista havaintoa liikenteestä tällä erää.', '2019-02-06', 'autot');

-- --------------------------------------------------------

--
-- Rakenne taululle `vastaus`
--

CREATE TABLE `vastaus` (
  `vastausID` int(11) NOT NULL,
  `kysymysID` int(11) NOT NULL,
  `nimimerkki` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `sisalto` varchar(500) COLLATE utf8_swedish_ci NOT NULL,
  `paivamaara` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Vedos taulusta `vastaus`
--

INSERT INTO `vastaus` (`vastausID`, `kysymysID`, `nimimerkki`, `sisalto`, `paivamaara`) VALUES
(3, 2, 'kissa', 'mee töihi', '2019-02-08'),
(4, 2, 'kissa', 'Sitäkös se onkin! \r\nJos ajelee Skodalla silmät kiinni ja uneksii niin voihan sitä kuvitella olevansa Audin ratissa.\r\nKuuleman mukaan Skodassa on paljon yhteistä (VAG) Audin kanssa, joten Audi-tunnelmaan pääsee.\r\nToista se on kuitenkin ihan oikealla Audilla ajeleminen, hiljaista ja mukavaa kyytiä.\r\n\r\nJuu, kaahailuvamma on yksilöllinen ja itse auto on siihen aivan syytön. Samanlaisia vammaisia ajelee eri merkeillä; sattuipahan vaan viime aikoina jotenkin useasti osumaan silmiin kaahailevat Skoda-k', '2019-02-08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kayttaja`
--
ALTER TABLE `kayttaja`
  ADD PRIMARY KEY (`kayttajaID`);

--
-- Indexes for table `kysymys`
--
ALTER TABLE `kysymys`
  ADD PRIMARY KEY (`kysymysID`);

--
-- Indexes for table `vastaus`
--
ALTER TABLE `vastaus`
  ADD PRIMARY KEY (`vastausID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kayttaja`
--
ALTER TABLE `kayttaja`
  MODIFY `kayttajaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `kysymys`
--
ALTER TABLE `kysymys`
  MODIFY `kysymysID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vastaus`
--
ALTER TABLE `vastaus`
  MODIFY `vastausID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
