<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <li><a href="index.php">Etusivu</a></li>
          <?php
            require_once "connect.php";

            session_start();

            if (isset($_SESSION['s_id'])) {
              echo '<li class="lista"><a href="post.php">Luo postaus</a></li>';
              echo '<li class="lista"><a href="logout.php">Kirjaudu ulos</a></li>';
            } else {
              echo "<li class='lista'><a href='login.php'>Kirjaudu</a></li>";
              echo "<li class='lista'><a href='register.php'>Rekisteröidy</a></li>";
            }
          ?>
        </ul>
      </div>

      <header>
        <h1>Kysymys palsta</h1>
        <p>Kysy ja joku vastaa!</p>
      </header>
      <?php
      if (isset($_SESSION['s_id'])) {
        echo '<p class="login-text"> teretulemast: ' . $_SESSION['s_nimi'];
      }
    ?>
    </p>
      <div class="categories">
        <ul>
          <li><a href="category.php?cat=autot">Autot</a></li>
          <li><a href="category.php?cat=harrastukset">Harrastukset</a></li>
          <li><a href="category.php?cat=matkailu">Matkailu</a></li>
          <li><a href="category.php?cat=ruoka-ja-juoma">Ruoka ja juoma</a></li>
          <li><a href="category.php?cat=terveys">Terveys</a></li>
          <li><a href="category.php?cat=urheilu">Urheilu ja kuntoilu</a></li>
          <li><a href="category.php?cat=viihde-ja-kulttuuri">Viihde ja kulttuuri</a></li>
          <li><a href="category.php?cat=jokin-muu">Jokin muu</a></li>
        </ul>
      </div>
    </div>
  </body>
</html>
