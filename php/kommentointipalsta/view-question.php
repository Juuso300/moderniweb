<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <li><a href="index.php">Etusivu</a></li>
          <?php
            require_once "connect.php";

            session_start();

            if (isset($_SESSION['s_id'])) {
              echo '<li class="lista"><a href="post.php">Luo postaus</a></li>';
              echo '<li class="lista"><a href="logout.php">Kirjaudu ulos</a></li>';
            } else {
              echo "<li class='lista'><a href='login.php'>Kirjaudu</a></li>";
              echo "<li class='lista'><a href='register.php'>Rekisteröidy</a></li>";
            }
          ?>
        </ul>
      </div>

      <header>
        <h1>
          <?php
            $sql = 'SELECT * FROM kysymys';

            if (isset($_GET['ID'])) {
              $cat = mysqli_real_escape_string($yhteys, strip_tags($_GET['ID']));
              $sql .= " WHERE kysymysID = '$cat'";
            }

            $tulokset = $yhteys->query($sql);
            if ($tulokset->num_rows>0) {
              $rivi = $tulokset->fetch_assoc();
              echo $rivi['otsikko'];

            echo "</h1>";
            echo "</header>";

            echo "<p class='date'>";
            echo $rivi['nimimerkki'] . ' ';
            $date = date_create($rivi['paivamaara']);
            echo date_format($date, 'd.m.Y');
            echo "</p>";
            echo "<p class='question_content'>";
            echo "<p>" . str_replace("\n","</p><p>", $rivi['sisalto']) . "</p>";
          }
          $sql2 = 'SELECT * FROM vastaus';

          if (isset($_GET['ID'])) {
            $cat2 = mysqli_real_escape_string($yhteys, strip_tags($_GET['ID']));
            $sql2 .= " WHERE kysymysID = '$cat2'";
          }

          $tulokset = $yhteys->query($sql2);
          if ($tulokset->num_rows>0) {
            while ($rivi2 = $tulokset->fetch_assoc()) {
              echo "<div class='commento'>";
              echo "<p class='date'>";
              echo $rivi2['nimimerkki'] . ' ';
              $date = date_create($rivi2['paivamaara']);
              echo date_format($date, 'd.m.Y');
              echo "</p>";
              echo "<p class='question_content'>";
              echo "<p>" . str_replace("\n","</p><p>", $rivi2['sisalto']) . "</p>";
              echo "</div>";
            }
          }
          ?>
        </p>
        <div class="comment">
          <form class="commenti" <?php echo "action= 'view-question.php?ID=" . $rivi['kysymysID'] . "'"; ?> method="post">
            Kirjoita kommentti: <br>
            <textarea name="comment" rows="8" cols="50"></textarea><br><br>
            <?php
              if (isset($_SESSION['s_id'])) {
                echo '<input type="submit" name="submit" value="Lähetä kommentti">';
              } else {
                echo "<p>Kirjaudu sisään kirjoittaaksesi kommentin</p>";
              }
            ?>
          </form>
          <?php
            if (isset($_GET['lahetys']) && $_GET['lahetys'] == 'onnistui' ) {
              echo "<p><b>Lähetys onnistui</b></p>";
            }

            if (isset($_POST['submit'])) {
              $sisalto = mysqli_real_escape_string($yhteys, strip_tags($_POST["comment"]));
              $nimi = $_SESSION['s_nimi'];
              $date = date('Y-m-d');
              if (isset($_GET['ID'])) {
                $ID = mysqli_real_escape_string($yhteys, strip_tags($_GET["ID"]));
              }

              if (empty($sisalto)) {
                echo "<p>Lähetys epäonnistui</p>";
              }

              $sql2 = "INSERT INTO vastaus (kysymysID, nimimerkki, sisalto, paivamaara)
              VALUES('$ID', '$nimi', '$sisalto', '$date');";
              mysqli_query($yhteys, $sql2);
              header('Location: view-question.php?ID=' . $rivi['kysymysID'] . '&lahetys=onnistui');
              exit();
            }
          ?>
        </div>
    </div>
  </body>
</html>
