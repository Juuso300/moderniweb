<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <li><a href="index.php">Etusivu</a></li>
          <?php
            require_once "connect.php";

            session_start();

            if (isset($_SESSION['s_id'])) {
              echo '<li class="lista"><a href="post.php">Luo postaus</a></li>';
              echo '<li class="lista"><a href="logout.php">Kirjaudu ulos</a></li>';
            } else {
              echo "<li class='lista'><a href='login.php'>Kirjaudu</a></li>";
              echo "<li class='lista'><a href='register.php'>Rekisteröidy</a></li>";
            }
          ?>
        </ul>
      </div>
      <header>
        <h1>Luo postaus</h1>
      </header>
      <div class="post">
        <form action="post.php" method="post">
          Otsikko: <br>
          <input type="text" name="otsikko" value=""><br>
          Sisältö: <br>
          <textarea name="sisalto" rows="8" cols="50"></textarea><br>
          Kategoria: <br>
          <select name="kategoria">
            <option value="" disabled selected>Valitse Kategoria</option>
            <option value="autot">Autot</option>
            <option value="harrastukset">Harrastukset</option>
            <option value="matkailu">Matkailu</option>
            <option value="ruokajajuoma">Ruoka ja juoma</option>
            <option value="terveys">Terveys</option>
            <option value="urheilujakuntoilu">Urheilu ja kuntoilu</option>
            <option value="viihdejakulttuuri">Viihde ja kulttuuri</option>
            <option value="jokinmuu">Jokin muu</option>
          </select><br><br>

          <input type="submit" name="submit" value="Lähetä">
        </form>
      </div>
      <?php
        if (isset($_GET['kategoria']) && $_GET['kategoria'] == 'valitse' ) {
          echo "<p><b>Valitse kategoria</b></p>";
        }

        if (isset($_GET['lahetys']) && $_GET['lahetys'] == 'onnistui' ) {
          echo "<p><b>Lähetys onnistui</b></p>";
        }

        if (isset($_POST['submit'])) {
          $otsikko = mysqli_real_escape_string($yhteys, strip_tags($_POST["otsikko"]));
          $sisalto = mysqli_real_escape_string($yhteys, strip_tags($_POST["sisalto"]));
          $kategoria = mysqli_real_escape_string($yhteys, strip_tags($_POST["kategoria"]));
          $nimi = $_SESSION['s_nimi'];
          $date = date('Y-m-d');

          if (empty($otsikko)) {
            $virheilmoitus = 'otsikko';

            header('location: post.php?' . $virheilmoitus);
            exit();
          }

          if ($kategoria == '') {
            header('location: post.php?kategoria=valitse');
            exit();
          }

          $sql2 = "INSERT INTO kysymys (nimimerkki, otsikko, sisalto, paivamaara, kategoria)
          VALUES('$nimi', '$otsikko', '$sisalto', '$date', '$kategoria');";
          mysqli_query($yhteys, $sql2);
          header('location: post.php?lähetys=onnistui');
        }
      ?>
    </div>
  </body>
</html>
