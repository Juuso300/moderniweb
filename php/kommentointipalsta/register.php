<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="wrapper">
      <div class="top-nav">
        <ul>
          <li class='lista'><a href="index.php">Etusivu</a></li>
          <?php
            session_start();

            if (isset($_SESSION['s_id'])) {
              echo '<li class="lista"><a href="logout.php">Kirjaudu ulos</a></li>';
              echo '<li class="lista"><a href="post.php">Luo postaus</a></li>';
            } else {
              echo "<li class='lista'><a href='login.php'>Kirjaudu</a></li>";
              echo "<li class='lista'><a href='register.php'>Rekisteröidy</a></li>";
            }
          ?>
        </ul>
      </div>

      <header>
        <h1>Rekisteröityminen</h1>
      </header>
      <div class="register">
        <form class="" action="register.php" method="post">
          Nimimerkki: <br>
          <input type="text" name="nimimerkki" value=""><br>
          Salasana: <br>
          <input type="password" name="salasana" value=""><br>
          Sähköposti: <br>
          <input type="email" name="sahkoposti" value=""><br><br>

          <input type="submit" name="submit" value="Lähetä">
        </form>
      </div>
      <?php

        require_once 'connect.php';

        if (isset($_POST["submit"])) {
          $nimimerkki = mysqli_real_escape_string($yhteys, $_POST["nimimerkki"]);
          $sahkoposti = mysqli_real_escape_string($yhteys, $_POST["sahkoposti"]);
          $salasana = mysqli_real_escape_string($yhteys, $_POST["salasana"]);

          if (empty($nimimerkki) || empty($sahkoposti) || empty($salasana) || strlen($salasana)<5) {
            $virheilmoitus = "";

            if (empty($nimimerkki) ){
              $virheilmoitus .= "nimi";
            }
            if (empty($sahkoposti)) {
              if (!empty($virheilmoitus)) {
                $virheilmoitus .= "&";
              }
              $virheilmoitus .= "sposti";
            }
            if (empty($salasana)) {
              if (!empty($virheilmoitus)) {
                $virheilmoitus .= "&";
              }
              $virheilmoitus .= "salasana";
            }
            if (!empty($salasana) && strlen($salasana)<5) {
              if (!empty($virheilmoitus)) {
                $virheilmoitus .= "&";
              }
              $virheilmoitus .= "Salasanalyhyt";
            }
            header("Location: register.php?" . $virheilmoitus);
            exit();
          }

          $sql = "SELECT * FROM kayttaja WHERE nimimerkki ='$nimimerkki'";
          $result = mysqli_query($yhteys, $sql);
          $resultcheck = mysqli_num_rows($result);

          if ($resultcheck > 0) {
            header("Location: register.php?signup=user");
            exit();
          } else {
            echo $salasana;
            $hashedsalasana = hash('sha256', $salasana);
            $sql2 = "INSERT INTO kayttaja (nimimerkki, sahkoposti, salasana)
            VALUES('$nimimerkki', '$sahkoposti', '$hashedsalasana');";
            mysqli_query($yhteys, $sql2);
            header("Location: register.php?signup=success");
            exit();
          }


        }
      ?>
    </div>
  </body>
</html>
