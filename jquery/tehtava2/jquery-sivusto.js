$(document).ready(function() {
  $('nav h3').click(function(){
        $(this).next('ul').toggle();
        if($(this).parent().attr('class') == "kiinni"){
          $(this).parent().attr("class", "auki");
        } else {
          $(this).parent().attr('class', "kiinni");
        }

    });

    $("#rekisteroityminen input").focus(function() {
      $(this).parent().children(".ohje").show();
    });
    $("#rekisteroityminen input").blur(function() {
      $(this).parent().children(".ohje").hide();
    });

    $("#salasanakentta1").keyup(function() {
      if ($(this).val().length < 8) {
        $(this).css("backgroundColor" , "#ffc9df");
      } else {
        $(this).css("backgroundColor" , "#cbffc9");
      }
      if ($(this).val() == $("#salasanakentta2").val()) {
        $("#salasanakentta2").css("backgroundColor" , "#cbffc9");
      } else {
        $("#salasanakentta2").css("backgroundColor" , "#ffc9df");
      }
    });

    $("#salasanakentta2").keyup(function() {
      if ($(this).val() == $("#salasanakentta1").val()) {
        $(this).css("backgroundColor" , "#cbffc9");
      } else {
        $(this).css("backgroundColor" , "#ffc9df");
      }
    });

    var i = 1;

    $("#sisalto li").click(function() {
      i = $(this).attr("data-tab");
      $("#tab1").css("display" , "none");
      $("#tab2").css("display" , "none");
      $("#tab3").css("display" , "none");
      $("valittu").css("display" , "block");

      $("#sisalto li").removeClass("valittu");
      $(this).addClass("valittu");

      if (i==1) {
        $("#tab1").css("display" , "block");
      } else if (i==2) {
        $("#tab2").css("display" , "block");
      } else {
        $("#tab3").css("display" , "block");
      }
    });

    var check = $("#hyvaksykayttoehdot");
        submitbutt = $("input[type='submit']");

    check.click(function () {
      submitbutt.attr("disabled" , false , check.is(":checked"));
    });

    check.click(function () {
      submitbutt.attr("disabled" , !check.is(":checked"));
    });

    submitbutt.click(function (){
      var ok = true;
      if ($("#nimimerkkikentta").val() == "") {
        $("#nimimerkkikentta").parent().children(".rlabel").css("color" , "#f20028");
        ok = false;
      } else {
        $("#nimimerkkikentta").parent().children(".rlabel").css("color" , "#000000");
      }
      if ($("#sahkopostikentta").val() == "") {
        $("#sahkopostikentta").parent().children(".rlabel").css("color" , "#f20028");
        ok = false;
      } else {
        $("#sahkopostikentta").parent().children(".rlabel").css("color" , "#000000");
      }
      if ($("#salasanakentta1").val() == "") {
        $("#salasanakentta1").parent().children(".rlabel").css("color" , "#f20028");
        ok = false;
      } else {
        $("#salasanakentta1").parent().children(".rlabel").css("color" , "#000000");
      }
      if ($("#salasanakentta2").val() == "") {
        $("#salasanakentta2").parent().children(".rlabel").css("color" , "#f20028");
        ok = false;
      } else {
        $("#salasanakentta2").parent().children(".rlabel").css("color" , "#000000");
      }
      return ok;
    });


    $("#lisaarivi").click(function (e) {
      e.preventDefault();
      $("#laskentataulukko").append('<tr><td></td><td></td><td></td><td></td><td></td><td class="poisto"><button>Poista</button></td></tr>');
    });
    $("#laskentataulukko").on("click", ".poisto", function(e){
      $(this).parent('tr').remove();
    });

    $("#laskentataulukko").on("dblclick" , "td" , function(e) {
      e.stopPropagation();
      $("#laskentataulukko td").attr("contenteditable" , false);
      $(this).attr("contenteditable" , true)
    });

    $("#laskentataulukko").on("keypress" , "td" , function(e) {
      var keycode = (event.keycode ? event.keycode : event.which);
      if (keycode == "13") {
        $("#laskentataulukko td").blur();
        event.stopPropagation();
      }
    });
});
